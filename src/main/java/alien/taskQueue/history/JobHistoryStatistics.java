package alien.taskQueue.history;

import java.math.BigDecimal;
import java.util.Map;
import java.util.logging.Level;

import alien.config.ConfigUtils;
import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import alien.monitoring.Timing;
import lazyj.DBFunctions;

/**
 * @author Elena Mihailescu
 * @since 2024-08-20
 */
public class JobHistoryStatistics extends HistoryConsumer {
	// Name of the processes.<table> to be updated
	private final String historyTable;
	static final Monitor monitor = MonitorFactory.getMonitor(JobHistoryStatistics.class.getCanonicalName());
	private final Integer weightFactor = 50;

	/**
	 * Constructor passing the target table name
	 *
	 * @param historyTable
	 */
	public JobHistoryStatistics(String historyTable) {
		super();

		logger.log(Level.INFO, "Starting new JobHistoryStatistics");
		this.historyTable = historyTable;

		monitor.addMonitoring("TQ_jobhistorystatistics_queue_info", (names, values) -> {
			names.add("TQ_jobhistorystatistics_queue_size");
			values.add(Integer.valueOf(queue.size()));
		});
	}

	/**
	 * Extracts fields from map (walltime, site, cpuModelName, productionId) and updates history database
	 * based on the Welford's algorithm for computing variance
	 *
	 * @param extrafields - map to process
	 */
	public void writeDB(Map<String, Object> extrafields) {
		try {
			Object map = extrafields.get("notifyMap");
			Long walltime = null;
			Long reqTTL = null;
			String site = null;
			String cpuModelName = null;
			Integer productionId = null;
			String endState = null;
			Integer cpuCores = null;

			// if we cannot get "Site" (see below), we try for CE
			if (extrafields.containsKey("CE"))
				site = extrafields.get("CE").toString();

			walltime = (Long) extrafields.get("runtimes");

			// TODO - solve unchecked and unsafe operations
			if (map instanceof Map) {
				final Map<?, ?> notifyMap = (Map<?, ?>) map;

				productionId = (Integer) notifyMap.get("productionId");
				site = (String) notifyMap.get("Site");
				cpuModelName = (String) notifyMap.get("cpuModelName");
				reqTTL = (Long) notifyMap.get("reqTTL");
				endState = (String) notifyMap.get("wrapperJobStatus");
				cpuCores = (Integer) notifyMap.get("cpuCores");

				// Update table only when we have site, cpuModelName and productionId
				if (endState != null && (endState.compareTo("DONE") == 0)
						&& site != null && cpuModelName != null
						&& productionId != null && walltime != null && walltime.longValue() > 0
						&& cpuCores != null && cpuCores.intValue() > 0) {

					try (Timing t = new Timing(monitor, "TQ_jobhistorystatistics_ms"); DBFunctions db = ConfigUtils.getDB("processes")) {
						if (db != null) {
							db.setQueryTimeout(60);

							int runtime = (int) (walltime.longValue() / cpuCores.intValue());
							/**
							 * Insert data into historyTable.
							 * If key (prodId,site, cpuModelName) does not exist:
							 * * insert prodId
							 * * insert site
							 * * insert cpuModelName
							 * * insert number of jobs (1 at first)
							 * * insert mean as the first value of spentTime
							 * * insert stddev as the first value of spentTime
							 * * insert M (helper value for the Welford's algorithm) as 0
							 * * insert first job spentTime as initial maxTime value
							 * * insert estimatedTTL as maxtime
							 * * update mean with another value (spentTime)
							 * If key exists:
							 * * mean, stddev, M, are updated according to Welford's method for computing variance
							 * * number of jobs is incremented
							 * * maxTime is updated only if the newer value is bigger and is not an outlier for the current dataset
							 * (new maxTime < mean + 3 * stddev)
							 * * estimatedTTL is computed as maxTime + 2 * stddev;
							 * Query example:
							 * INSERT INTO TTLPredictionHistorySite(prodId, site, cpuModelName, jobs, mean, stddev, welfordM, maxTime, estimatedTTL)
							 * VALUES (123, "Test", "Test", 1, 30, 30, 0, 30) ON DUPLICATE KEY UPDATE
							 * welfordM = welfordM + ((@time:=30) - mean)*(@time - (jobs * mean + @time)/(jobs+1)),
							 * mean=(jobs * mean + @time)/(jobs + 1), jobs=jobs + 1, stddev=sqrt(welfordM/jobs),
							 * maxTime=IF(stddev = 0, maxTime, IF((@time - mean)/stddev > 3, maxTime,
							 * IF(maxTime > @time, maxTime, @time))), estimatedTTL=CAST(maxTime + 2 * stddev AS UNSIGNED)
							 */

							db.query("INSERT INTO " + historyTable
									+ "(prodId, site, cpuModelName, jobs, mean, stddev, welfordM, maxTime, estimatedTTL) "
									+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE "
									+ "welfordM = welfordM + ((@time:=?) - mean)*(@time - (jobs * mean + @time)/(jobs+1)), "
									+ "mean=(jobs * mean + @time)/(jobs + 1), "
									+ "jobs=jobs + 1, "
									+ "stddev=sqrt(welfordM/jobs), "
									+ "maxTime=IF(stddev = 0, maxTime, IF((@time - mean)/stddev > 3, maxTime, IF(maxTime > @time, maxTime, @time))), "
									+ "estimatedTTL=CAST(maxTime + 2 * stddev AS UNSIGNED INTEGER)",
									false,
									productionId,
									site,
									cpuModelName,
									Integer.valueOf(1),
									new BigDecimal(runtime),
									new BigDecimal(runtime),
									new BigDecimal(0),
									Integer.valueOf(runtime),
									new BigDecimal(runtime),
									new BigDecimal(runtime));

							logger.log(Level.INFO, "[History] Added in " + historyTable + " walltime: " + walltime
									+ "; cpuCores = " + cpuCores + "; site: " + site
									+ "; cpuModelName: " + cpuModelName + "; productionId: " + productionId
									+ "; reqTTL: " + reqTTL);
						}
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			else {
				logger.log(Level.SEVERE, "History data could not be processed: " + map);
			}
		}
		catch (Exception e) {
			logger.log(Level.WARNING, "Exception processing " + extrafields, e);
		}
	}

	@Override
	public void processData(Map<String, Object> data) {

		logger.log(Level.INFO, "[History]  We have something in queue to process: " + data);
		try {
			writeDB(data);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Long getEstimatedTTL(Map<String, Object> request) {
		Long reqTTL = null;
		String site = null;
		String cpuModelName = null;
		Integer productionId = null;

		reqTTL = (Long) request.get("reqTTL");
		site = (String) request.get("Site");
		cpuModelName = (String) request.get("cpuModelName");
		productionId = Integer.valueOf((String) request.get("productionId"));

		if (reqTTL == null || site == null || cpuModelName == null || productionId == null) {
			logger.log(Level.SEVERE, "[History] Cannot optimize TTL. Malformed request received: " +  request);
			return null;
		}

		try (Timing t = new Timing(monitor, "TQ_jobhistorystatisticsrequest_ms")) {

			try {
				DBFunctions db = ConfigUtils.getDB("processes");
				if (db != null) {
					db.setQueryTimeout(60);
					db.setReadOnly(true);

					db.query("SELECT jobs, estimatedTTL from " + historyTable
							+ " WHERE prodId=? and site=? and cpuModelName=?;",
							false,
							productionId,
							site,
							cpuModelName
							);

					logger.log(Level.INFO, "[History] Requested jobs and estimated TTL for ("
							+ productionId + ", " + site + ", " + cpuModelName + ")");

					if (db.moveNext()) {
						final String[] columns = db.getColumnNames();

						for (final String col : columns) {
							logger.log(Level.INFO, "[History] Getting " + col + "-" + db.gets(col));

						}
						Integer jobs = db.geti("jobs");
						Long estimatedTTL = db.getl("estimatedTTL");
						double weight = (double) weightFactor / (weightFactor + jobs);
						Long weightedTTL = (long) (weight * reqTTL + (1 - weight) * estimatedTTL);

						if (weightedTTL < reqTTL)
							return weightedTTL;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		return null;
	}




}
