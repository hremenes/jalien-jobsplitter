package alien.taskQueue.history;

/**
 * @author Elena Mihailescu
 * @since 2024-08-20
 */
import java.util.Map;
import java.util.logging.Level;

public class JobHistoryFactory extends HistoryFactory {
	private JobHistoryStatistics jobHistoryStatistics;

	/**
	 * Default constructor
	 */
	public JobHistoryFactory() {
		super();

		jobHistoryStatistics = new JobHistoryStatistics("TTLPredictionHistorySite");
		this.addConsumer(jobHistoryStatistics);

		// TODO - Add other JobHistoryConsumers
	}

	public Long getEstimatedTTL(String ttlOptimizationType, Map<String, Object> request) {

		switch (ttlOptimizationType) {
		case "TTLStatistics": {
			return jobHistoryStatistics.getEstimatedTTL(request);
		}
		default:
			logger.log(Level.SEVERE, "Unexpected ttlOptimizationType: " + ttlOptimizationType);
		}

		return null;
	}

}
