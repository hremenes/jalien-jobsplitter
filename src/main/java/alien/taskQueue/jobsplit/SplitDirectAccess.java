package alien.taskQueue.jobsplit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import alien.catalogue.LFN;

import alien.taskQueue.JDL;


/**
 * @author Haakon
 */
public class SplitDirectAccess extends JobSplitter {

	/**
	 * queue id for masterjob
	 */
	private long masterId;

	/**
	 * list of subjobs to submit
	 */
	private final List<JDL> subjobs = new ArrayList<>();

	/**
	 * @param strategy
	 */
	SplitDirectAccess(final String strategy) {
		super.strategy = strategy;
	}

	Collection<LFN> groupInputFiles(final JDL mJDL) throws IOException, IllegalArgumentException {

		final Collection<LFN> inputFiles = mJDL.getInputLFNs();

		checkNoDownload();

		if (inputFiles.isEmpty())
			throw new IOException("Could not find inputdata files!");

		for (final LFN lfn : inputFiles) {
			lfn.lfn = getFinalLfnString(lfn.getCanonicalName());
		}
		return inputFiles;
	}

	@Override
	List<JDL> splitJobs(final JDL mJDL, final long lMasterId) throws IOException, IllegalArgumentException {

		masterJdl = mJDL;
		this.masterId = lMasterId;
		final Collection<LFN> inputFiles = groupInputFiles(mJDL);
		List<String> tempInput = new ArrayList<>();

		int maxinputsize = 0;
		int maxinputnumber = 0;

		if (mJDL.gets("SplitMaxInputFileNumber") != null) {
			maxinputnumber = Integer.parseInt(mJDL.gets("SplitMaxInputFileNumber"));
		}

		else if (mJDL.gets("SplitMaxInputFileSize") != null) {
			maxinputsize = Integer.parseInt(mJDL.gets("SplitMaxInputFileSize"));
		}
		else
			throw new IllegalArgumentException("Split by AF is required to set either max number or max size of input files per subjob");

		int currentSize = 0;

		final JDL baseJDL = prepareSubJobJDL(mJDL, lMasterId);

		if (mJDL.gets("OrderLFN") != null) {
			sortStrategy = mJDL.gets("SplitMaxInputFileNumber");
		}

		tempInput = new ArrayList<>();

		if (maxinputsize > 0) {
			for (final LFN lfn : inputFiles) {

				if (maxinputsize < currentSize + lfn.getSize() && !tempInput.isEmpty()) {
					addSubjob(baseJDL, tempInput, currentSize);
					tempInput = new ArrayList<>();
					currentSize = 0;
				}

				currentSize += lfn.getSize();
				tempInput.add(lfn.lfn);

			}

		}

		if (maxinputnumber != 0) {

			int currentnumber = 0;
			int numberGroups = (int) Math.ceil((double) inputFiles.size() / maxinputnumber);
			if (numberGroups == 0)
				numberGroups = 1;

			int tmpMax = inputFiles.size() / numberGroups;

			for (final LFN lfn : inputFiles) {

				if (currentnumber >= tmpMax && currentnumber != 0) {
					numberGroups--;
					tmpMax = ((inputFiles.size() - currentnumber) / numberGroups) + currentnumber;
					addSubjob(baseJDL, tempInput, currentSize);
					tempInput = new ArrayList<>();

				}
				tempInput.add(lfn.lfn);
				currentnumber++;
			}
		}

		if (!tempInput.isEmpty()) {
			addSubjob(baseJDL, tempInput, currentSize);
		}

		return subjobs;
	}

	private void addSubjob(final JDL baseJDL, final List<String> tempInput, final int currentSize) throws IllegalArgumentException {
		final JDL tmpJdl = changeFilePattern(baseJDL, tempInput);
		tmpJdl.addRequirement(setSizeRequirements(currentSize));
		tmpJdl.set("InputData", tempInput);
		subjobs.add(tmpJdl);
	}

}
