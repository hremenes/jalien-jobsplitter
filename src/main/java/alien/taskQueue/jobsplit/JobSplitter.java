package alien.taskQueue.jobsplit;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import alien.catalogue.LFN;
import alien.config.ConfigUtils;
import alien.taskQueue.JDL;
import alien.taskQueue.TaskQueueUtils;

/**
 * @author Haakon
 * @since 2024-07-01
 */
public abstract class JobSplitter {

	/**
	 * Number of subjobs produced
	 */
	protected int counter = 1;

	boolean bNoDownload = false;
	List<String> noDownloadList = new ArrayList<>();

	/**
	 * JDL-specified splitting strategy
	 */
	protected String strategy;

	protected String sortStrategy;
	/**
	 *
	 */
	protected Map<String, Object> collectionChangeString = new HashMap<>();
	/**
	 *
	 */
	protected Map<String, List<Object>> collectionChangeLambda = new HashMap<>();
	/**
	 * Masterjob JDL
	 */
	protected JDL masterJdl;
	/**
	 *
	 */
	protected long space = 0;
	/**
	 *
	 */
	protected String sizeReq;

	/**
	 *
	 */
	public JobSplitter() {

	}

	abstract List<JDL> splitJobs(final JDL j, long masterId) throws IOException, SQLException;

	/**
	 * Functional interface used with lambda to replace #alien# filename
	 */
	@FunctionalInterface
	private interface inputfileAlien {
		String run(List<String> a);
	}

	/**
	 * Functional interface used with lambda to replace #alien# counter
	 */
	@FunctionalInterface
	private interface counterAlien {
		int run(int counter);
	}

	void checkQuota(final int numberFiles, final int maxinputperjob) throws IllegalArgumentException, IOException {
		if (masterJdl.gets("User") != null) {
			final String quotaResponse = TaskQueueUtils.checkJobQuota(masterJdl.gets("User"), numberFiles / maxinputperjob);

			if (!quotaResponse.equals("Allowed")) {
				throw new IOException(quotaResponse);
			}
		}
		else {
			throw new IllegalArgumentException("User does not exist as a field in the JDL");
		}
	}

	/**
	 * Prepare base JDL for subjobs before split
	 *
	 * @param mJDL masterjob JDL
	 * @param masterId queue id for masterjob
	 * @return base JDL for subjobs final touches
	 * @throws IOException
	 */
	JDL prepareSubJobJDL(final JDL mJDL, final long masterId) throws IllegalArgumentException {
		final JDL tmpJdl;
		masterJdl = mJDL;

		tmpJdl = new JDL();

		final String newArg = mJDL.gets("SplitArguments");
		if (newArg != null) {

			String arg = "";
			if (mJDL.gets("Arguments") != null)
				arg = mJDL.gets("Arguments") + " ";

			tmpJdl.set("Arguments", arg + newArg);
			masterJdl.set("Arguments", arg + newArg);
			masterJdl.delete("SplitArguments");

		}

		tmpJdl.set("Splitted", strategy);
		tmpJdl.set("MasterJobID", Long.toString(masterId));

		prepareSizeRequirements(mJDL.gets("WorkDirectorySize"));
		checkEntryPattern();

		return tmpJdl;
	}

	/**
	 * Go through masterjob JDL looking for #alien# patterns to replace
	 *
	 * @throws IllegalArgumentException
	 */
	public void checkEntryPattern() throws IllegalArgumentException {

		final Set<String> jdlKeys = masterJdl.keySet();
		String updatedValue;
		for (final String key : jdlKeys) {
			if (!key.startsWith("LPM")) {
				final Object keyValue = masterJdl.get(key);

				if (keyValue instanceof String) {

					updatedValue = checkFilePattern(keyValue.toString(), key);
					if (!"".equals(updatedValue)) {
						collectionChangeString.put(key, updatedValue);
					}
				}

				else if (keyValue instanceof Collection) {
					final Object[] newValues = ((Collection<?>) keyValue).toArray();
					boolean change = false;

					for (int i = 0; i < newValues.length; i++) {
						if (newValues[i] instanceof String) {
							updatedValue = checkFilePattern(newValues[i].toString(), key);
							if (!"".equals(updatedValue)) {
								change = true;
								newValues[i] = updatedValue;
							}
						}
					}
					if (change)
						collectionChangeString.put(key, newValues);
				}
			}
		}

	}

	/**
	 * Replacing #alien# patterns with lambda to be applied after job is split
	 *
	 * @param origString Original string related to a JDL key
	 * @param jdlKey Relevant JDL key
	 * @return new string after replacing #alien#
	 * @throws IllegalArgumentException
	 */
	public String checkFilePattern(final String origString, final String jdlKey) throws IllegalArgumentException {

		final List<Object> function = new ArrayList<>();

		if (!origString.contains("#alien")) {
			return "";
		}

		String newString = origString.replace("%", "%%");
		final String origPattern = "#alien([^#]*)";
		final Pattern p = Pattern.compile(origPattern, Pattern.CASE_INSENSITIVE);
		final Matcher m = p.matcher(origString);
		while (m.find()) {
			String val = m.group(1);
			String method = "";
			String newPattern = "";

			if (val.matches("^first")) {
				method = "First";
				val = val.replace("first", "");
			}
			else if (val.matches("^last")) {
				method = "Last";
				val = val.replace("last", "");
			}
			else if (val.matches("^all")) {
				method = "All";
				val = val.replace("all", "");
			}

			if (val.matches("^fulldir$")) {
				if ("Last".equals(method)) {
					final inputfileAlien alienfulldir = (final List<String> l) -> {
						String s = l.get(l.size() - 1);
						s = s.replace("LF:", "");
						s = s.replace(",nodownload", "");
						return s;
					};
					function.add(alienfulldir);

				}
				else {
					final inputfileAlien alienfulldir = (final List<String> l) -> {
						String s = l.get(0);
						s = s.replace("LF:", "");
						s = s.replace(",nodownload", "");
						return s;
					};
					function.add(alienfulldir);
				}
				newPattern = "%s";
			}
			else if (val.matches("^dir$")) {
				if ("Last".equals(method)) {
					final inputfileAlien alienfulldir = (final List<String> l) -> {
						String s = l.get(l.size() - 1);
						s = s.replace("LF:", "");
						s = s.replace(",nodownload", "");
						s = s.replaceFirst("/[^/]*$", "");
						s = s.replaceFirst("^.*/", "");
						return s;
					};
					function.add(alienfulldir);
				}
				else {
					final inputfileAlien alienfulldir = (final List<String> l) -> {
						String s = l.get(0);
						s = s.replace("LF:", "");
						s = s.replace(",nodownload", "");
						s = s.replaceFirst("/[^/]*$", "");
						s = s.replaceFirst("^.*/", "");
						return s;
					};
					function.add(alienfulldir);
				}
				newPattern = "%s";
			}
			else if (val.matches("^filename.*")) {
				final Pattern p2 = Pattern.compile("^filename(/(.*)/(.*)/)?$", Pattern.CASE_INSENSITIVE);
				final Matcher m2 = p2.matcher(val);

				if (m2.find()) {
					String before = "";
					String after = "";
					if (m2.group(1) != null) {
						before = m2.group(2);
						after = m2.group(3);
					}

					final Pattern finBefore = Pattern.compile(before);
					final String finAfter = after;

					if ("Last".equals(method)) {

						final inputfileAlien alienfulldir = (final List<String> l) -> {
							String s = l.get(l.size() - 1);
							s = s.replace("LF:", "");
							s = s.replace(",nodownload", "");
							s = s.replaceFirst(".*/", "");
							s = finBefore.matcher(s).replaceAll(finAfter);
							return s;
						};
						function.add(alienfulldir);
					}

					else if ("All".equals(method)) {

						final inputfileAlien alienfulldir = (final List<String> l) -> {
							String s = String.join(",", l);
							s = s.replace("LF:", "");
							s = s.replace(",nodownload", "");
							s = finBefore.matcher(s).replaceAll(finAfter);
							return s;
						};

						function.add(alienfulldir);
					}

					else {
						final inputfileAlien alienfulldir = (final List<String> l) -> {
							String s = l.get(0);
							s = s.replace("LF:", "");
							s = s.replace(",nodownload", "");
							s = s.replaceFirst(".*/", "");
							s = finBefore.matcher(s).replaceAll(finAfter);
							return s;
						};
						function.add(alienfulldir);
					}
					newPattern = "%s";
				}
			}
			else {
				final String pCounter = "^_counter_?(.*)$";
				final Pattern p2 = Pattern.compile(pCounter, Pattern.CASE_INSENSITIVE);
				final Matcher m2 = p2.matcher(val);
				if (m2.find()) {
					if (!"".equals(m2.group(1))) {
						newPattern = "%" + m2.group(1).replace("i", "d");
					}
					else {
						newPattern = "%d";
					}
					final counterAlien alienCounter = (final int cnt) -> {
						return cnt;
					};

					function.add(alienCounter);
				}
			}

			if ("".equals(newPattern))
				throw new IllegalArgumentException("#alien# pattern does not match anything");

			newString = newString.replaceFirst("#alien[^#]*#", newPattern);
		}
		collectionChangeLambda.put(jdlKey, function);

		return newString;
	}

	/**
	 * Apply lambda functions to get final string after #alien# replacement
	 *
	 * @param baseJDL base subjob JDL before individual subjob changes
	 * @param inputlist List of inputdata files
	 * @return New JDL after #alien# changes are made
	 * @throws IllegalArgumentException
	 */
	public JDL changeFilePattern(final JDL baseJDL, final List<String> inputlist) throws IllegalArgumentException {
		final JDL newJDL;
		try {
			newJDL = new JDL(baseJDL);
		}
		catch (final IOException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
		for (final Map.Entry<String, Object> entry : collectionChangeString.entrySet()) {
			boolean keyCollection = false;
			String key = entry.getKey();
			Object value = entry.getValue();
			String fullString = "";
			if (value instanceof String[]) {
				fullString = String.join(";;;", (String[]) value);
				keyCollection = true;

			}
			else
				fullString = (String) collectionChangeString.get(key);

			final Object[] tmpLambda = new Object[collectionChangeLambda.get(key).size()];
			int i = 0;

			for (final Object lambda : collectionChangeLambda.get(key)) {
				if (lambda instanceof inputfileAlien) {
					final inputfileAlien tmpVal = (inputfileAlien) lambda;
					tmpLambda[i] = tmpVal.run(inputlist);
				}
				else {
					final counterAlien tmpVal = (counterAlien) lambda;
					tmpLambda[i] = Integer.valueOf(tmpVal.run(counter));

				}
				i++;
			}

			fullString = String.format(fullString, tmpLambda);
			if (keyCollection) {
				final String[] tmpArray = fullString.split(";;;");
				newJDL.set(key, tmpArray);

			}
			else
				newJDL.set(key, fullString);
		}

		counter++;
		return newJDL;
	}

	/**
	 * Prepare for size requirements before splitting
	 *
	 * @param workSpace String of JDL key WorkDirectorySize
	 * @throws IllegalArgumentException
	 */
	public void prepareSizeRequirements(final String workSpace) throws IllegalArgumentException {

		if (workSpace != null) {
			try {
				if (workSpace.toUpperCase().contains("MB")) {
					space = Long.parseLong(workSpace.replaceAll("MB$", ""));
					space = space * 1024;
				}

				else if (workSpace.toUpperCase().contains("GB")) {
					space = Integer.parseInt(workSpace.replaceAll("GB$", ""));
					space = space * 1024 * 1024;
				}
				else
					throw (new IllegalArgumentException("JDL field WorkDirectorySize: " + workSpace + ", does not contain 'MB' or 'GB'"));
			}
			catch (final Exception e) {
				throw (new IllegalArgumentException("Could not get WorkDirectorySize: " + e.getClass().toString() + " " + e.getMessage()));
			}

		}

		sizeReq = ("( ( other.LocalDiskSpace > %d ) )");
	}

	/**
	 * Find and set size requirements for subjobs
	 *
	 * @param filesize size of inputdata files if applicable
	 * @return New Requirements string for subjob JDL
	 */
	String setSizeRequirements(final int filesize) {
		return String.format(sizeReq, Long.valueOf(findSizeRequirements(filesize)));
	}

	/**
	 * Find size requirement
	 *
	 * @param l size of inputdata files if applicable
	 * @return filesize requirement
	 */
	long findSizeRequirements(final long l) {
		long filesize = l;

		if (filesize != 0) {
			filesize = ((filesize / (1024 * 8192)) + 1) * 8192;
		}
		if (space > filesize) {
			filesize = space;
		}

		return filesize;
	}

	void checkNoDownload() {

		if (masterJdl.get("InputDataCollection") != null) {
			bNoDownload = masterJdl.gets("InputDataCollection").indexOf(",nodownload") > 0;
		}
		else if (masterJdl.get("InputData") != null) {
			final Object o = masterJdl.get("InputData");
			if (o instanceof Collection<?>) {
				for (final Object o2 : (Collection<?>) o)
					if (o2 instanceof String) {
						final String s = (String) o2;
						if (s.indexOf(",nodownload") > 0) {
							if (!bNoDownload)
								bNoDownload = true;
							noDownloadList.add(s);
						}
					}
			}

			else if (o instanceof CharSequence) {
				final String s = o.toString();
				if (s.indexOf(",nodownload") > 0) {
					if (!bNoDownload)
						bNoDownload = true;
					noDownloadList.add(s);
				}
			}

		}
	}

	String getFinalLfnString(final String lfn) {
		String fullJdl = "LF:" + lfn;
		if (bNoDownload) {
			if (noDownloadList.isEmpty())
				fullJdl = fullJdl + ",nodownload";
			else {
				for (final String s : noDownloadList) {
					if (lfn.equals(s)) {
						fullJdl = fullJdl + ",nodownload";
						break;
					}
				}
			}
		}
		return fullJdl;
	}

	public List<LFN> sortLFNs (List<LFN> input){
			switch (sortStrategy.toLowerCase()) {
				case "random":
					Collections.shuffle(input);
					break;
				case "alphabetical":
					Collections.sort(input, Comparator.comparing(LFN::getCanonicalName));
					break;
				case "size":
					input.sort(Comparator.comparingLong(LFN::getSize));
					break;
				case "balance":
					input = sortBalance(input);
					break;
				default:
					input = sortName(input,sortStrategy.toLowerCase());
					break;
			}
		return input;
	}

	List<LFN> sortBalance(List<LFN> input){
		input.sort(Comparator.comparingLong(LFN::getSize));

		List<LFN> sortedAlternating = new ArrayList<>();

		int left = 0;
		int right = input.size() - 1;
		boolean pickFromRight = true;

		while (left <= right) {
			if (pickFromRight) {
				sortedAlternating.add(input.get(right--));
			} else {
				sortedAlternating.add(input.get(left++));
			}
			pickFromRight = !pickFromRight;
		}

		return sortedAlternating;
	}

	List<LFN> sortName(List<LFN> input, String sPat){
			String regex = String.format("_%s(\\d+)", sPat);

			Pattern pattern = Pattern.compile(regex);

			// Custom comparator to compare strings based on the extracted part
			Comparator<LFN> comparator = (s1, s2) -> {
				Matcher m1 = pattern.matcher(s1.getCanonicalName());
				Matcher m2 = pattern.matcher(s2.getCanonicalName());

				if (m1.find() && m2.find()) {
					int num1 = Integer.parseInt(m1.group(1));
					int num2 = Integer.parseInt(m2.group(1));
					return Integer.compare(num1, num2);
				} else {
					// Handle the case where the pattern is not found in one or both strings
					return s1.compareTo(s2);
				}
			};

		// Sort the list using the custom comparator
		Collections.sort(input, comparator);
		return input;
	}


}


