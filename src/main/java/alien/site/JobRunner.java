package alien.site;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

import alien.api.DispatchSSLClient;
import alien.api.taskQueue.TaskQueueApiUtils;
import alien.config.ConfigUtils;
import alien.config.Version;
import alien.monitoring.Monitor;
import alien.monitoring.MonitorFactory;
import alien.shell.commands.JAliEnCOMMander;
import apmon.ApMon;
/**
 * @author sweisz
 * @since Mar 25, 2021
 */
public class JobRunner extends JobAgent {

	/**
	 * logger object
	 */
	private static final Logger logger = ConfigUtils.getLogger(JobRunner.class.getCanonicalName());

	/**
	 * ML monitor object
	 */
	static final Monitor monitor = MonitorFactory.getMonitor(JobRunner.class.getCanonicalName());

	static {
		monitor.addMonitoring("resource_status", (names, values) -> {
			names.add("totalcpu");
			values.add(Long.valueOf(JobAgent.slotResources.getCPU()));

			names.add("availablecpu");
			values.add(Long.valueOf(JobAgent.runningResources.getCPU()));

			names.add("allocatedcpu");
			values.add(Long.valueOf(JobAgent.slotResources.getCPU() - JobAgent.runningResources.getCPU()));

			names.add("runningja");
			values.add(Long.valueOf(JobAgent.RUNNING_JOBAGENTS));

			names.add("slotlength");
			values.add(Long.valueOf(JobAgent.slotResources.getTtl()));

			names.add("totaloversubscribed");
			values.add(Long.valueOf(JobAgent.slotResources.getOversubscribedCores()));

			names.add("allocatedoversubscribedcpu");
			values.add(Long.valueOf(JobAgent.slotResources.getOversubscribedCores() - JobAgent.runningResources.getOversubscribedCores()));
		});
	}

	/**
	 * Oversubscription
	 */
	long WAIT_OVERSUBSCRIBE = 240; // We will wait at least this time for starting a new oversubscribed job
	long OVERLOADED_MAXTIME = 120; // We tolerate 2 min of high CPU pressure
	long IDLE_CPU_LACK = 200; // If we only have 2 cores available --> abort
	long IDLE_CPU_OVERSUBSCRIBE = 300; // Free cores to start oversubscription workflow
	boolean enoughMemToOversubscribe = false;
	static Stack<JobAgent> oversubscribedJobs;

	/**
	 * Timestamps
	 */
	long timestamp = System.currentTimeMillis() / 1000, lastLaunchTimestamp = System.currentTimeMillis() / 1000 - (4 * 60);
	long idleTimestamp = 0;
	long overloadedTimestamp = 0;
	long compilationTimestamp = Version.getCompilationTimestamp();

	/**
	 * Resources
	 */
	int threads = 0;
	double cpuIdle = 0;
	boolean cpuIsolated = false;

	/**
	 * Process & ttl
	 */
	final int jobRunnerPid = MonitorFactory.getSelfProcessID();
	final int maxRetries = Integer.parseInt(System.getenv().getOrDefault("MAX_RETRIES", "2"));
	final long ttlEnd = timestamp + JobAgent.slotResources.getTtl();

	/** 
	*/
	public static void main(final String[] args) {
		ConfigUtils.setApplicationName("JobRunner");
		ConfigUtils.switchToForkProcessLaunching();

		DispatchSSLClient.setIdleTimeout(30000);

		final JobRunner jr = new JobRunner();
		jr.run();
	}

	@Override
	public void run() {
		remoteDebugging = TaskQueueApiUtils.getRemoteDebugging(System.getenv().get("CE"), ConfigUtils.getLocalHostname(), compilationTimestamp);
		if (remoteDebugging) {
			ConfigUtils.setDebugFlag(true);
			logger.log(Level.INFO, "Debug flag set to " + remoteDebugging);
		}

		collectSystemInformation();

		oversubscribedJobs = new Stack<>();
		JobAgent.runningResources.setOversubscribedCores(0l);
		JobAgent.slotResources.setOversubscribedCores(0l);

		if (cpuOversubscription && wholeNode)
			setupOversubscriptionPool();

		CgroupUtils.setupTopCgroups(jobRunnerPid);

		boolean JRNotified = false;
		while (timestamp < ttlEnd) {
			synchronized (JobAgent.requestSync) {
				try {
					if (cpuOversubscription && wholeNode)
						checkOversubscription();
					
					// Check if we can launch new JA
					if (JRNotified || (timestamp - lastLaunchTimestamp) > getElapsedTime()) {
						lastLaunchTimestamp = timestamp;
						JRNotified = false;

						if (hasSlotResources(false)) {
							launchJobAgent(jobRunnerPid, false, threads, 0);
							logger.log(Level.INFO, "Spawned thread nr " + threads);

							idleTimestamp = 0;
							threads++;
						}
						else {
							monitor.sendParameter("state", "All slots busy");

							if (cpuOversubscription && wholeNode)
								tryOversubscribe();
							else
								logger.log(Level.INFO, "No new thread");

							monitor.sendParameter("statenumeric", Long.valueOf(3));

							if (threads == 0) {
								logger.log(Level.INFO, "No agents were able to start. Exiting...");
								System.exit(0);
							}
						}
					}

					timestamp = System.currentTimeMillis() / 1000;

					JobAgent.requestSync.wait(30 * 1000);
					if (((System.currentTimeMillis() / 1000) - timestamp) < 30 * 1000) { // If JR has been notified will be less than 30s
						logger.log(Level.INFO, "JR notified. Starting new JA");
						JRNotified = true;
					}

					monitor.incrementCounter("startedja");
					monitor.sendParameter("retries", Long.valueOf(JobAgent.retries.get()));
					monitor.sendParameter("remainingttl", Long.valueOf(ttlEnd - timestamp));

					if (JobAgent.retries.get() >= maxRetries && JobAgent.runningResources.getOversubscribedCores() == JobAgent.slotResources.getOversubscribedCores()) {
						JAliEnCOMMander.getInstance().q_api.getPinningInspection(new byte[JobAgent.RES_NOCPUS.intValue()], true, ConfigUtils.getLocalHostname());
						
						monitor.sendParameter("state", "Last JA cannot get job");
						monitor.sendParameter("statenumeric", Long.valueOf(2));

						logger.log(Level.INFO, "JobRunner going to exit from lack of jobs");
						System.exit(0);
					}
				}
				catch (final Exception e) {
					logger.log(Level.WARNING, "JobRunner main loop caught an exception", e);

					if (e instanceof InterruptedException)
						break;
				}
			}
		}

		logger.log(Level.INFO,"JobRunner Exiting");
		System.exit(0);
	}

	private JobAgent launchJobAgent(int jrPid, boolean oversubscribed, int i, int coresToOversubscribe) {
		JobAgent ja = new JobAgent();
		Thread jaThread = new Thread(ja, "JobAgent_" + i);
		jaThread.start();

		ja.setOversubscription(oversubscribed);
		ja.setOversubscribedAvailable(coresToOversubscribe);
		if (cpuIsolation && !cpuIsolated) {
			cpuIsolated = checkAndApplyIsolation(jrPid, cpuIsolated);
		}

		monitor.sendParameter("state", "Waiting for JA to get a job");
		monitor.sendParameter("statenumeric", Long.valueOf(1));

		return ja;
	}

	private void setupOversubscriptionPool(){
		double memoryPerCore = 0;
		double swapPerCore = 0;

		try {
			memoryPerCore = (MonitorFactory.getApMonSender().getSystemParameter("mem_free").doubleValue() + MonitorFactory.getApMonSender().getSystemParameter("mem_used").doubleValue()) / JobAgent.slotResources.getCPU();
			swapPerCore = (MonitorFactory.getApMonSender().getSystemParameter("swap_free").doubleValue() + MonitorFactory.getApMonSender().getSystemParameter("swap_used").doubleValue()) / JobAgent.slotResources.getCPU();
			logger.log(Level.INFO, "Memory per core " + memoryPerCore);
			logger.log(Level.INFO, "SWAP per core " + swapPerCore);

			enoughMemToOversubscribe = memoryPerCore > 2 * 1024;
			logger.log(Level.INFO, "Oversubscription - By memory would add " + Math.floor(memoryPerCore/(2*1024)) + " cores");

			long oversubscribedCores = (long) Math.floor(memoryPerCore / (2 * 1024));
			JobAgent.runningResources.setOversubscribedCores(oversubscribedCores);
			JobAgent.slotResources.setOversubscribedCores(oversubscribedCores);
			logger.log(Level.INFO, "Oversubscription - OVERSUBSCRIBED_CPU set to  " + JobAgent.runningResources.getOversubscribedCores());
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Could not get the node's memory to set up the oversubscription pool", e);
			cpuOversubscription = false;
		}
	}

	private void checkOversubscription(){
		ApMon.updateCPUComponents();

		try {
			cpuIdle = Double.parseDouble(ApMon.getIdleCPU());
		}
		catch (NumberFormatException nfe) {
			logger.log(Level.SEVERE, "Could not fetch machine's idle CPU", nfe);
			cpuIdle = 0;
		}

		if (JobAgent.runningResources.getOversubscribedCores() < JobAgent.slotResources.getOversubscribedCores()) {
			// Checking if it might be impacting other jobs
			if (cpuIdle * JobAgent.slotResources.getCPU() < IDLE_CPU_LACK) {
				logger.log(Level.INFO, "We lack idle. Current idle = " + cpuIdle + "%");

				if (overloadedTimestamp == 0)
					overloadedTimestamp = System.currentTimeMillis() / 1000;
				logger.log(Level.INFO, "In overloaded for " + (timestamp - overloadedTimestamp) + " seconds");

				if (overloadedTimestamp != 0 && (timestamp - overloadedTimestamp) >= OVERLOADED_MAXTIME && !oversubscribedJobs.empty()) {
					JobAgent toKill = oversubscribedJobs.pop();
					String status = toKill.getWrapperJobStatus();

					if ("RUNNING".equals(status) || "SAVING".equals(status)) {
						logger.log(Level.INFO, "Oversubscription - Time to kill. process " + toKill.getQueueId());
						toKill.killForcibly(toKill.getJobWrapperProcess());
						
						logger.log(Level.INFO, "Oversubscription -  Going to change job status back to WAITING");
						putJobTrace("Job kill on oversubscription regime. Putting back to WAITING.");
						
						overloadedTimestamp = 0;
						WAIT_OVERSUBSCRIBE += 10 * 60; // Prevent black-holes
					}
				}
			}
			else if (cpuIdle * JobAgent.slotResources.getCPU() > IDLE_CPU_LACK && overloadedTimestamp != 0) {
				overloadedTimestamp = 0;
				logger.log(Level.INFO, "Reseting overloaded counter. CPU idle = " + cpuIdle + "%");
			}
		}
	}
	
	private boolean tryOversubscribe() {
		if (enoughMemToOversubscribe && cpuIdle * JobAgent.slotResources.getCPU() >= IDLE_CPU_OVERSUBSCRIBE) {
			logger.log(Level.INFO, "We have enough idle. Current idle = " + cpuIdle + "%");

			if (idleTimestamp == 0)
				idleTimestamp = System.currentTimeMillis() / 1000;

			logger.log(Level.INFO, "In idle for " + (System.currentTimeMillis() / 1000 - idleTimestamp) + " seconds.");

			if (idleTimestamp != 0 && (timestamp - idleTimestamp) >= WAIT_OVERSUBSCRIBE) {
				logger.log(Level.INFO, "Starting oversubscription workflow");

				if (hasSlotResources(true)) {
					int coresToOversubscribe = (int) (cpuIdle * JobAgent.slotResources.getCPU() / 100 - 2); // Leaving two cores for the system
					if (coresToOversubscribe > JobAgent.runningResources.getOversubscribedCores())
						coresToOversubscribe = (int) JobAgent.runningResources.getOversubscribedCores();

					JobAgent ja = launchJobAgent(jobRunnerPid, true, threads, coresToOversubscribe);
					logger.log(Level.INFO, "Spawned thread nr " + threads + " (Oversubscribed). Can use up to " + coresToOversubscribe + " oversubscription pool");
					oversubscribedJobs.push(ja);
					
					threads++;
					idleTimestamp = 0;
					WAIT_OVERSUBSCRIBE = 240;

					return true;
				}
			}
		}
		else if (cpuIdle * JobAgent.slotResources.getCPU() < IDLE_CPU_OVERSUBSCRIBE && idleTimestamp != 0) {
			idleTimestamp = 0;
			logger.log(Level.INFO, "Reseting idle counter. CPU idle = " + cpuIdle + "%");
		}
		return false;
	}

	private long getElapsedTime() {
		if ((ttlEnd - timestamp) < 60 * 60 * 12 && slotResources.getCPU() > runningResources.getCPU()) {
			return (long) (-0.086 * (ttlEnd - timestamp) + 3910); // until last 12h -> 3'. Then start decreasing until at the last 1h -> 60'
		}
		else {
			return 3l * 60;
		}
	}

	/**
	 * Moves the oversubscribed job to the regular job pool
	 */
	static void moveExtraJobToRegularPool(long freedCPU) {
		JobAgent ja = null;
		Iterator<JobAgent> it = oversubscribedJobs.iterator();
		while (it.hasNext()) {
			ja = it.next();
			if (ja.getReqCPU() <= freedCPU) {
				break;
			}
		}
		if (ja != null) {
			logger.log(Level.INFO, "Moving oversubscribed job to regular pool " + ja.getQueueId() + " (using "  + ja.getReqCPU() + " cores)");
			ja.setOversubscription(false);
			runningResources.addOversubscribedCPU(ja.getReqCPU());
			runningResources.substractCPU(ja.getReqCPU());
			logger.log(Level.INFO, "Current regular pool has now " + runningResources.getCPU() + " CPU cores. Oversubscribed pool has " + JobAgent.runningResources.getOversubscribedCores() + " cores");

			if (cpuIsolation) {
				if (CgroupUtils.hasCgroupsv2() && CgroupUtils.hasController(ja.agentCgroupV2, "cpu") )
					ja.limitCPUcgroupsV2();
				else {
					String isolCmd = ja.addIsolation();
					NUMAExplorer.applyTaskset(isolCmd, ja.getChildPID());
					logger.log(Level.INFO, "Job " + ja.getQueueId() + " pinned to mask " + isolCmd);
				}
			}

			ja.putJobTrace("Moving agent to the regular resource pool");

			oversubscribedJobs.remove(ja);
		}
	}

	/**
	 * Gets the JA sorter by different strategies to record in DB
	 *
	 * @param slotMem Total memory consumed in the slot
	 * @param slotMemsw
	 * @param reason
	 * @param parsedSlotLimit
	 */
	public static void recordHighestConsumer(double slotMem, double slotMemsw, String reason, double parsedSlotLimit) {
		SorterByAbsoluteMemoryUsage jobSorter1 = new SorterByAbsoluteMemoryUsage();
		sortByComparator(slotMem, slotMemsw, jobSorter1, reason, parsedSlotLimit, true);
		SorterByRelativeMemoryUsage jobSorter2 = new SorterByRelativeMemoryUsage();
		sortByComparator(slotMem, slotMemsw, jobSorter2, reason, parsedSlotLimit, false);
		SorterByTemporalGrowth jobSorter3 = new SorterByTemporalGrowth();
		sortByComparator(slotMem, slotMemsw, jobSorter3, reason, parsedSlotLimit, false);
	}

	private static void sortByComparator(double slotMem, double slotMemsw, Comparator<JobAgent> jobSorter, String reason, double parsedSlotLimit, boolean realPreemption) {
		ArrayList<JobAgent> sortedJA = new ArrayList<>(MemoryController.activeJAInstances.values());
		for (JobAgent ja : sortedJA)
			ja.checkProcessResources(false);
		JobAgent toPreempt = null;
		long preemptionTs = System.currentTimeMillis();
		if (!oversubscribedJobs.isEmpty()) {
			JobAgent toPreemptCandidate = oversubscribedJobs.pop();
			if (toPreemptCandidate.getQueueId() > 0)
				toPreempt = toPreemptCandidate;
		} else {
			String sorterId = jobSorter.getClass().getCanonicalName().split("\\.")[jobSorter.getClass().getCanonicalName().split("\\.").length -1];
			Collections.sort(sortedJA, jobSorter);
			if (MemoryController.debugMemoryController) {
				logger.log(Level.INFO, "Sorted jobs with " + sorterId + ": ");
				for (JobAgent ja : sortedJA)
					logger.log(Level.INFO, "Job " + ja.getQueueId() + " consuming VMEM " + ja.usedResources.getVMEM() + " MB and RMEM " + ja.usedResources.getRMEM() + " MB RAM");
			}
			int i = 0;
			while (i < sortedJA.size()) {
				JobAgent ja = sortedJA.get(i);
				if (ja.usedResources.getVMEM() > ja.requiredResources.getRMEM()) { //Here we make sure we do not kill a job that is consuming less than 2G per slot
					toPreempt = ja;
					break;
				}
				i += 1;
			}
		}

		if (toPreempt != null && !toPreempt.alreadyPreempted) {
			for (JobAgent ja : sortedJA) {
				boolean success = ja.recordPreemption(preemptionTs, slotMem, slotMemsw, ja.usedResources.getVMEM(), reason, parsedSlotLimit, sortedJA.size(), toPreempt.getQueueId(),realPreemption);
				if (!success) {
					logger.log(Level.INFO, "Could not record preemption on central DB");
				}
			}
			MemoryController.preemptionRound += 1;
		}
		else if (toPreempt == null) {
			logger.log(Level.INFO, "Could not start preemption. All running jobs in the slot were consuming less than 2GB/core");
		}
	}
}
