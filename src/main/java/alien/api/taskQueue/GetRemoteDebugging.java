package alien.api.taskQueue;

import java.util.Arrays;
import java.util.List;

import alien.api.Request;
import alien.taskQueue.TaskQueueUtils;

/**
 * Get debugging to the CS is enabled
 *
 * @author marta
 * @since
 */
public class GetRemoteDebugging extends Request {
	/**
	 *
	 */
	private static final long serialVersionUID = 5022083696413315513L;

	private String site;
	private String host;
	private long buildTs;

	private boolean remoteDebuggingFlag;

	/**
	 * @param sitename 
	 * @param hostname 
	 * @param ts 
	 */
	public GetRemoteDebugging(String sitename, String hostname, long ts) {
		site = sitename;
		host = hostname;
		buildTs = ts;
	}

	@Override
	public List<String> getArguments() {
		return Arrays.asList();
	}

	@Override
	public void run() {
		remoteDebuggingFlag = TaskQueueUtils.getRemoteDebugging(site, host, buildTs);

	}

	/**
	 * @return whether or not this instance should send logs to central services 
	 */
	public boolean getDebuggingFlag() {
		return remoteDebuggingFlag;
	}

	@Override
	public String toString() {
		return "";
	}
}
