package alien.optimizers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.stream.Collectors;

import alien.config.ConfigUtils;
import alien.taskQueue.Job;
import alien.taskQueue.JobStatus;
import alien.taskQueue.TaskQueueUtils;
import alien.taskQueue.jobsplit.JobSplitHelper;
import lazyj.DBFunctions;
import lazyj.DBFunctions.DBConnection;

/**
 * @author Haakon
 * @since 2024-07-01
 */
public class JobOptimizer extends Optimizer {
	/**
	 * Size of threadpool for splitting
	 */
	private static final int MAX_POOL_SIZE = ConfigUtils.getConfig().geti("alien.optimizers.joboptimizer.poolsize", 1);

	private static final int SLEEP_SECONDS = ConfigUtils.getConfig().geti("alien.optimizers.joboptimizer.sleep", 5);
	/**
	 * threadpool used for spliting with custom  handler if full
	 */
	private static ThreadPoolExecutor executor;

	/**
	 * @return <code>true</code> if the instance can split itself, <code>false</code> if not
	 */
	public static boolean isEnabled() {
		return MAX_POOL_SIZE > 0;
	}

	@Override
	public void run() {

		if (MAX_POOL_SIZE == 0) {
			logger.log(Level.INFO, "Job Optimizer not enabled at this host, stopping it");
			return;
		}

		executor = new ThreadPoolExecutor(MAX_POOL_SIZE, MAX_POOL_SIZE, 1, TimeUnit.HOURS, new SynchronousQueue<>(), new ExecutorHandler());

		this.setSleepPeriod(SLEEP_SECONDS * 1000); // 30 seconds (1 minute) temporarily
		final String selectOldQuery = "SELECT queueId FROM QUEUE WHERE statusId = "
				+ JobStatus.SPLITTING.getAliEnLevel() + " AND split = 0 AND mtime < (CURRENT_TIMESTAMP - INTERVAL 1 HOUR)"
				+ " LIMIT %d FOR UPDATE";
		final String updateOldQuery = "UPDATE QUEUE SET "
				+ "mtime = NOW() WHERE queueId IN (%s)";
		final String selectQuery = "SELECT queueId FROM QUEUE WHERE statusId = "
				+ JobStatus.INSERTING.getAliEnLevel() + " AND split = 0"
				+ " LIMIT %d FOR UPDATE";
		final String updateQuery = "UPDATE QUEUE SET "
				+ "statusId = " + JobStatus.SPLITTING.getAliEnLevel() + ", mtime = NOW() WHERE queueId IN (%s)";

		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			logger.info("Did it even go inside shutdown?");
			executor.shutdown();
			try {
				if(executor.awaitTermination(1000, TimeUnit.MILLISECONDS))
					logger.info("Shutdown gracefully");

				else
					logger.info("Did not shutdown gracefully");



			}
			catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
			logger.info("Did it even shutdown?");
		}));


		while (true) {

			logger.log(Level.FINER, "Job Optimizer is awake!");

			try (DBFunctions db = ConfigUtils.getDB("processes")) {
				if (db == null) {
					logger.log(Level.WARNING, "Job Optimizer could not get a DB!");
					return;
				}
				db.setReadOnly(false);
				final DBConnection dbc = db.getConnection();
				final Connection conn = dbc.getConnection();

				try {
					dbc.setReadOnly(false);
					conn.setAutoCommit(false);

					int activeT = executor.getActiveCount();
					int emptySlots = MAX_POOL_SIZE - activeT;
					if (emptySlots > 0) {
						List<Long> queueIdListOld = new ArrayList<>();
						List<Long> queueIdListNew = new ArrayList<>();

						try (Statement stat = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE)) {
							stat.setQueryTimeout(120);

							try (final ResultSet resultUpdateOldSet = stat.executeQuery(String.format(selectOldQuery,Integer.valueOf(emptySlots)))) {
								while (resultUpdateOldSet.next()) {
									queueIdListOld.add(Long.valueOf(resultUpdateOldSet.getLong(1)));
									emptySlots--;
								}
							}
							if (!queueIdListOld.isEmpty()) {
								String queueIdString = queueIdListOld.stream().map(Object::toString).collect(Collectors.joining(", "));
								int a = stat.executeUpdate(String.format(updateOldQuery, queueIdString));
								if (a == 0) {
									throw new Exception("updateOldQuery did not update anything");
								}
							}

							if(emptySlots > 0) {
								try (final ResultSet resultUpdateSet = stat.executeQuery(String.format(selectQuery,Integer.valueOf(emptySlots)))) {
									while (resultUpdateSet.next()) {
										queueIdListNew.add(Long.valueOf(resultUpdateSet.getLong(1)));
									}
								}
								if (!queueIdListNew.isEmpty()) {
									String queueIdString = queueIdListNew.stream().map(Object::toString).collect(Collectors.joining(", "));
									int a = stat.executeUpdate(String.format(updateQuery, queueIdString));
									if (a == 0) {
										throw new Exception("updateQuery did not update anything");
									}
								}
							}
							conn.commit();
						} catch (Exception e) {
							conn.rollback();
							throw e;
						}
						for (long queueId:queueIdListOld){
							TaskQueueUtils.putJobLog(queueId, "trace", "Stuck in SPLITTING for a while, retrying", null);
							try {
								logger.log(Level.FINER, "queueId to be split is: " + queueId);
								final Job mJob = TaskQueueUtils.getJob(queueId);

								JobSplitHelper jobSplit = new JobSplitHelper(mJob);
								jobSplit.setCleanupCheck();

								executeJobSplit(jobSplit);


							}
							catch (Exception e) {
								logger.log(Level.INFO, "Job Optimizer could not split: " + e.getMessage());
							}
						}
						for (long queueId:queueIdListNew){
							TaskQueueUtils.putJobLog(queueId, "state", "Job state transition from INSERTING to SPLITTING", null);
							TaskQueueUtils.sendJobStatusToML(queueId, JobStatus.SPLITTING, "NO_SITE");

							try {
								logger.log(Level.FINER, "queueId to be split is: " + queueId);
								final Job mJob = TaskQueueUtils.getJob(queueId);

								JobSplitHelper jobSplit = new JobSplitHelper(mJob);

								executeJobSplit(jobSplit);


							}
							catch (Exception e) {
								logger.log(Level.INFO, "Job Optimizer could not split: " + e.getMessage());
							}
						}
					}
				}
				catch (Exception e) { //SQLException e
					logger.log(Level.INFO, "Job Optimizer could not execute statement: " + e.getMessage());

				}
				finally {
					try {
						conn.setAutoCommit(true);
						dbc.free();
					} catch (@SuppressWarnings("unused") SQLException e){
						dbc.close();
					}
				}
			}
			catch (final Exception e) {
				logger.log(Level.INFO, "Job optimizer stopped working, " + e.getMessage());
			}

			logger.log(Level.FINER, "Job Optimizer goes to sleep....zZzZz");

			try {
				sleep(this.getSleepPeriod());
			}
			catch (InterruptedException e) {
				throw new RuntimeException(e);
			}

		}
	}

	/**
	 * @param jobsplit job to split
	 */
	public static void executeJobSplit(JobSplitHelper jobsplit) {
		executor.execute(jobsplit);
	}

	private static class ExecutorHandler implements RejectedExecutionHandler {
		@Override
		public void rejectedExecution(Runnable r, ThreadPoolExecutor executorRef) {
			JobSplitHelper j = (JobSplitHelper) r;
			j.setJobStatus();
			logger.log(Level.INFO, "Cannot add a job to the threadpool queue as it is full! Transition to INSERTING");

		}
	}
}



